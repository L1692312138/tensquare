package com.lsh.controller;

import com.lsh.model.Label;
import com.lsh.model.User;
import com.lsh.service.Impl.MapperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/5 3:13 下午
 * @desc ：
 */
@RequestMapping("/mybatis")
@RestController
public class BaseMapperController {
    @Autowired
    MapperService service;
    @GetMapping
    public List<Label> findAll(){
        return service.findAll();
    }
    @GetMapping("/id")
    public Label findById(String id){
        return service.findById(id);
    }
    @GetMapping("/findByKeyword")
    public List<Label> findByKeyword(String labelname){
        return service.findByKeyword(labelname);
    }
    @PostMapping("/findByKeywords")
    public List<Label> findByKeywords(@RequestBody Label label){
        return service.findByKeywords(label);
    }
    @PostMapping
    public void add(@RequestBody Label label){
         service.add(label);
    }
    @PutMapping
    public void update(@RequestBody Label label){
         service.upadte(label);
    }
    @DeleteMapping
    public void del(String  id){
         service.deleteById(id);
    }
    @GetMapping("/findLabelByMapperXml")
    public List<Label> findByMapperXml(){
        return service.findLabelByMapperXml();
    }
    @GetMapping("/findUserByMapperXml")
    public List<User> findUserByMapperXml(){
        return service.findUserByMapperXml();
    }


}
