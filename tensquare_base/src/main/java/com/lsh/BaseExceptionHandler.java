package com.lsh;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/28 9:16 下午
 * @desc ：
 */

import com.lsh.util.ResultObject;
import com.lsh.util.StatusCode;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author QiuShiju
 * @Desc 统一异常处理
 */
@ControllerAdvice
public class BaseExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResultObject error(Exception e) {
        e.printStackTrace();
        return  new ResultObject(false, StatusCode.ERROR,e.getMessage());
    }

}


