package com.lsh.mapper.db2;

import com.lsh.model.User;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/5 3:55 下午
 * @desc ：
 */
public interface UserMapper {
    List<User> findAllLabelByMapperXML() ;
}
