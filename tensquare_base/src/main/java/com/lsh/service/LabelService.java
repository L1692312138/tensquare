package com.lsh.service;

import com.lsh.model.Label;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/28 9:01 下午
 * @desc ：
 */
public interface LabelService {
    List<Label> findAll();

    Label findById(String labelId);

    void add(Label label);

    void update(Label label);

    void deleteById(String labelId);

    List<Label> findSearch(Label label);

    Page findSearchPage(Label label, int page, int size);
}
