package com.lsh.model;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 2:53 下午
 * @desc ：
 */

import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * 不使用JPA 所有不需要设置@Entity和@Table
 */
@Data
public class Spit implements Serializable {
    @Id
    private String _id;
    private String content;
    private Date publishtime;
    private String userid;
    private String nickname;
    private Integer visits;
    private Integer thumbup;
    private Integer share;
    private Integer comment;
    private String state;
    private String parentid;
}
