package com.lsh.service.impl;

import com.lsh.model.Spit;
import com.lsh.repository.SpitRepository;
import com.lsh.service.SpitService;
import com.lsh.util.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 2:59 下午
 * @desc ：
 */
@Service
@Transactional
public class SpitServiceImpl implements SpitService {
    @Autowired
    SpitRepository repository;

    @Autowired
    IdWorker idWorker;

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public List<Spit> findAll() {
        System.out.println("SpitServiceImpl");
        return repository.findAll();
    }

    @Override
    public Spit findById(String id) {
        return repository.findById(id).get();
    }

    @Override
    public void save(Spit spit) {
        repository.save(spit);
    }

    @Override
    public void update(Spit spit) {
        repository.save(spit);
    }

    @Override
    public void deleteById(String id) {
        repository.deleteById(id);
    }

    @Override
    public void incVisist(String spitId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(spitId));
        Update update = new Update();
        update.inc("visist", 1);
        mongoTemplate.updateFirst(query,update,"spit");
    }
}
