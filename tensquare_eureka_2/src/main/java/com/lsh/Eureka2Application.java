package com.lsh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/28 9:37 下午
 * @desc ：
 */
@SpringBootApplication
@EnableEurekaServer
@EnableEurekaClient
public class Eureka2Application {
    public static void main(String[] args) {
        SpringApplication.run(Eureka2Application.class);
    }
}
