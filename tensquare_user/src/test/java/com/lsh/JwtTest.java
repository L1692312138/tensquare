package com.lsh;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/3 2:27 下午
 * @desc ：
 */
@SpringBootTest
public class JwtTest {
    public static final String key = "zhiyou";
    public static final Integer ttl = 10000;

    @Test
    public void test(){
        String jwt = createJWT("1", "张三", "admin", "delete_anything");
        System.out.println("jwt:"+jwt);
        Claims claims = parseJWT(jwt);
        System.out.println("claims:"+claims.toString());
    }

    public String createJWT(String id, String subject, String roles,String permission) {
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        JwtBuilder builder = Jwts.builder()
                .setId(id)
                .setSubject(subject)
                .setIssuedAt(now)
                .signWith(SignatureAlgorithm.HS256, key)
                .claim("roles", roles)
                .claim("permission",permission);
        if (ttl > 0) {
            builder.setExpiration( new Date( nowMillis + ttl));
        }
        return builder.compact();
    }
    public Claims parseJWT(String jwtStr){
        return  Jwts.parser()
                .setSigningKey(key)
                .parseClaimsJws(jwtStr)
                .getBody();
    }
}