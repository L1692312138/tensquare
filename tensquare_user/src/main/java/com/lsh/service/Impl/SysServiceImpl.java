package com.lsh.service.Impl;

import com.lsh.model.*;
import com.lsh.repository.*;
import com.lsh.service.SysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/9 2:53 下午
 * @desc ：
 */
@Service
public class SysServiceImpl implements SysService {
    /**
     * 用户表
     */
    @Autowired
    SysUserRepository sysUserRepository;
    /**
     * 角色表
     */
    @Autowired
    SysRoleRepository sysRoleRepository;

    /**
     * 用户角色中间表
     */
    @Autowired
    SysUserRoleRepository sysUserRoleRepository;
    /**
     * 角色权限中间表
     */
    @Autowired
    SysRolePermissionRepository sysRolePermissionRepository;

    /**
     * 权限表
     */
    @Autowired
    SysPermissionRepository sysPermissionRepository;
    /**
     * 加解密
     */
    @Autowired
    private BCryptPasswordEncoder encoder;

    /**
     * 根据用户名查询用户信息   查询用户表
     * @param username
     * @return
     */
    @Override
    public SysUser findUserByUsername(String username) {
        SysUser sysUser = sysUserRepository.findSysUsersByName(username);
        return sysUser;
    }

    /**
     * 根据用户ID查询角色ID   查询用户角色中间表
     * @param id
     * @return
     */
    @Override
    public List<SysUserRole> findRolesIdByUserID(Integer id) {
        List<SysUserRole> sysUserRolesByUserId = sysUserRoleRepository.findSysUserRolesByUserId(id);
        return sysUserRolesByUserId;
    }

    /**
     * 根据角色ID查询角色信息   查询角色表
     * @param roleId
     * @return
     */
    @Override
    public SysRole findRoleByRoleId(Integer roleId) {
        return sysRoleRepository.findSysRolesById(roleId);
    }

    /**
     * 根据角色Name查询角色信息   查询角色表
     * @param roleName
     * @return
     */
    @Override
    public SysRole findRoleIdBuRoleName(String roleName) {
        return sysRoleRepository.findSysRoleByName(roleName);
    }

    /**
     * 根据角色ID查询权限ID   一对多
     * @param id
     * @return
     */
    @Override
    public List<SysRolePermission> findSysRolePermissionByRoleId(Integer id) {
        return sysRolePermissionRepository.findSysRolePermissionByRoleId(id);
    }

    /**
     * 根据权限ID查询权限
     * @param permissionId
     * @return
     */
    @Override
    public SysPermission findPermissionByPermessionId(Integer permissionId) {
        return sysPermissionRepository.findSysPermissionById(permissionId);
    }


    /**
     * 添加用户
     * @param sysUser
     */
    @Override
    public void addSysUser(SysUser sysUser) {
        sysUser.setPassword(encoder.encode(sysUser.getPassword()));
        sysUserRepository.save(sysUser);
    }
}
