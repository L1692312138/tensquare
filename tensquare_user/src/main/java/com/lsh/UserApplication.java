package com.lsh;

import com.lsh.util.IdWorker;
import com.lsh.util.JwtUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/28 8:36 下午
 * @desc ：
 */
//@EnableEurekaClient
    //开启Session共享
@EnableRedisHttpSession
@SpringBootApplication
public class UserApplication {
    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class);
    }

    /**
     * 注入雪花算法
     * @return
     */
    @Bean
    public IdWorker getIdWorkder() {
        return new IdWorker(1,1);
    }

    /**
     * security
     * @return
     */
    @Bean
    public BCryptPasswordEncoder encoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public JwtUtil getJwtUtil(){
        return new JwtUtil();
    }

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

}
