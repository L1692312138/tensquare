package com.lsh.repository;

import com.lsh.model.SysUserRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/9 2:55 下午
 * @desc ：
 */
public interface SysUserRoleRepository extends JpaRepository<SysUserRole,Integer> {
    List<SysUserRole>  findSysUserRolesByUserId(int userID);
}
