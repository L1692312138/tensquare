package com.lsh.repository;

import com.lsh.model.SysRole;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/9 2:53 下午
 * @desc ：
 */
public interface SysRoleRepository extends JpaRepository<SysRole,Integer> {
    /**
     * 根据 角色id查询角色
     * @param id
     * @return
     */
    SysRole findSysRolesById (Integer id);

    /**
     * 根绝角色name 查询角色
     * @param name
     * @return
     */
    SysRole findSysRoleByName(String name);

}
