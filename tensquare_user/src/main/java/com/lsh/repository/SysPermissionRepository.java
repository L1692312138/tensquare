package com.lsh.repository;

import com.lsh.model.SysPermission;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/9 2:55 下午
 * @desc ：
 */
public interface SysPermissionRepository extends JpaRepository<SysPermission,Integer> {
    SysPermission findSysPermissionById(Integer id);
}
