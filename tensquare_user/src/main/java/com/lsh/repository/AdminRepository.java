package com.lsh.repository;

import com.lsh.model.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/28 9:03 下午
 * @desc ：
 */
public interface AdminRepository extends JpaRepository<Admin,String>, JpaSpecificationExecutor<Admin> {
    Admin findByLoginname(String loginname);
}
