package com.lsh.repository;

import com.lsh.model.SysRolePermission;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/9 2:55 下午
 * @desc ：
 */
public interface SysRolePermissionRepository extends JpaRepository<SysRolePermission, Integer> {
//    @Query(value = "SELECT * FROM `sys_role_permission` WHERE `role_id` = ?",nativeQuery = true)
    List<SysRolePermission> findSysRolePermissionByRoleId(Integer roleId);
}
