package com.lsh.repository;

import com.lsh.model.SysUser;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/9 2:55 下午
 * @desc ：
 */
public interface SysUserRepository extends JpaRepository<SysUser,Integer> {
    SysUser findSysUsersByName(String name);
}
