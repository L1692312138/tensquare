package com.lsh.config;

import com.lsh.interceptor.JwtInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/3 4:52 下午
 * @desc ：将JwtInterceptor注册到Spring容器   JWT 拦截器
 */
//@Configuration
public class InterceptorConfig extends WebMvcConfigurationSupport {

    @Autowired
    private JwtInterceptor jwtInterceptor;
    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        //注册拦截器要声明拦截器对象和要拦截的请求
        registry.addInterceptor(jwtInterceptor)
                //拦截所有请求 进入JwtInterceptor拦截器
                .addPathPatterns("/**")
                //排除登陆请求  和 /
                .excludePathPatterns("/**/login/**")
                .excludePathPatterns("/")
                .excludePathPatterns("/**/**.css");
    }

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        //重写这个方法，映射静态资源文件
        registry.addResourceHandler("/**")
                .addResourceLocations("classpath:/resources/")
                .addResourceLocations("classpath:/static/")
                .addResourceLocations("classpath:/public/");
        super.addResourceHandlers(registry);    }
}

