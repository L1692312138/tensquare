package com.lsh.service.impl;

import com.lsh.model.Article;
import com.lsh.repository.ArticleRepository;
import com.lsh.service.ArticleService;
import com.lsh.util.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 9:58 上午
 * @desc ：
 */
@Service
public class ArticleServiceImpl implements ArticleService {
    @Autowired
    ArticleRepository repository;
    @Autowired
    RedisTemplate redisTemplate;
    @Autowired
    IdWorker idWorker;



    @Override
    public void updateState(String id) {
        repository.updateState(id);
    }

    @Override
    public void addThumbup(String id) {
        repository.addThumbup(id);
    }

    @Override
    public Article findById(String id) {
        //先从缓存中查询当前对象
        Article article = (Article) redisTemplate.opsForValue().get("article:" + id);
        System.out.println(article);
        //如果没有取到
        if (article == null) {
            //从数据库中查询
            article = repository.findById(id).get();
            redisTemplate.opsForValue().set("article:" + id, article,60*3, TimeUnit.SECONDS);
        }
        return article;
    }

    @Override
    public void add(Article article) {
        String articleId = idWorker.nextId() + "";
        article.setId(articleId);
        repository.save(article);
    }

    /**
     * 条件搜索加分页
     * @param
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public Page<Article> search(Article article, int pageNum, int pageSize) {
        Pageable pageable = PageRequest.of(pageNum-1,pageSize);
        Page<Article> articles = repository.findAll(new Specification<Article>() {
            @Override
            public Predicate toPredicate(Root<Article> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> list = new ArrayList<>();
                String articleTitle = article.getTitle();
                if (articleTitle != null && !"".equals(articleTitle)) {
                    Predicate title = criteriaBuilder.like(root.get("title").as(String.class), "%" + articleTitle + "%");
                    list.add(title);
                }
                Predicate[] predicates = list.toArray(new Predicate[list.size()]);
                return criteriaBuilder.and(predicates);
            }
        }, pageable);
        return articles;
    }
}
