package com.lsh.controller;

import com.lsh.model.Article;
import com.lsh.service.ArticleService;
import com.lsh.util.PageResult;
import com.lsh.util.ResultObject;
import com.lsh.util.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 10:00 上午
 * @desc ：
 */
@RefreshScope
@RestController
@CrossOrigin
@RequestMapping("/article")
public class ArticleController {

    @Autowired
    ArticleService articleService;

    @PostMapping("/search/{pageNum}/{pageSize}")
    public ResultObject searchAndPage(@RequestBody Article article,@PathVariable("pageNum") int pageNum,@PathVariable("pageSize") int pageSize){
        Page<Article> articlePage = articleService.search(article,pageNum,pageSize);
        return new ResultObject(true, StatusCode.OK, "搜索成功", new PageResult(articlePage.getTotalElements(),articlePage.getContent()));
    }
    @PostMapping
    public ResultObject add(@RequestBody Article article){
        articleService.add(article);
        return new ResultObject(true, StatusCode.OK, "添加文章成功");
    }

    @PutMapping(value = "/examine/{articleId}")
    public ResultObject examine(@PathVariable String articleId) {
        articleService.updateState(articleId);
        return new ResultObject(true, StatusCode.OK, "审核成功");
    }

    @PutMapping(value = "/thumbup/{articleId}")
    public ResultObject thumbup(@PathVariable String articleId) {
        articleService.addThumbup(articleId);
        return new ResultObject(true, StatusCode.OK, "点赞成功");
    }
}

