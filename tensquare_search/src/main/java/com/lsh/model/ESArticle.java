package com.lsh.model;

import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.Id;
import java.io.Serializable;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/30 10:14 上午
 * @desc ：
 */
// @Document声明es的文档类型
// indexName es的所索引库名字
// type  es的类型[表]
@Data
@Document(indexName = "tensquare_article", type = "article")
public class ESArticle implements Serializable {

    @Id
    private String id;

    //是否索引，就是看该域是否能被搜索。
    //是否分词，就表示搜索的时候是整体匹配还是单词匹配
    //是否存储，就是是否在页面上显示
    @Field(index = true, analyzer = "ik_max_word", searchAnalyzer = "ik_max_word",type = FieldType.Text)
    private String title;

    @Field(index = true, analyzer = "ik_max_word", searchAnalyzer = "ik_max_word",type = FieldType.Text)
    private String content;

    // 状态用于当mysql中删除数据时es更新不及时,做查询判断用
    private String state;//审核状态
}