package com.lsh.service;

import com.lsh.model.ESArticle;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/30 10:16 上午
 * @desc ：
 */
public interface ArticleService {

    void save(ESArticle article);

    List<ESArticle> findAll();

    Page<ESArticle> findByKey(String key, int page, int size);
}
