package com.lsh.service;

import com.lsh.model.Recruit;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 9:30 上午
 * @desc ：
 */
public interface RecruitService {
    List<Recruit> findTop2ByStateOrderByCreatetimeDesc();
    List<Recruit> newJob();
}
