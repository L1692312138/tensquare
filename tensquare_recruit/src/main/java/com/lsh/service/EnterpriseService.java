package com.lsh.service;

import com.lsh.model.Enterprise;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 9:22 上午
 * @desc ：
 */
public interface EnterpriseService {
    List<Enterprise> findByIshot(String isHot);
}
