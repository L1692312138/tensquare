package com.lsh.service;

import com.lsh.model.Problem;
import org.springframework.data.domain.Page;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 9:42 上午
 * @desc ：
 */
public interface ProblemService {

    Page<Problem> newlist(String labelid, int page, int rows);
    Page<Problem> hotlist(String labelid, int page, int rows);
    Page<Problem> waitlist(String labelid, int page, int rows);

}
