package com.lsh.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 9:35 上午
 * @desc ：
 */
@Data
@Entity
@Table(name="tb_problem")
public class Problem implements Serializable {

    @Id
    private String id;
    private String title;
    private String content;
    private java.util.Date createtime;
    private java.util.Date updatetime;
    private String userid;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 浏览量
     */
    private Long visits;
    /**
     * 点赞数
     */
    private Long thumbup;
    /**
     * 回复数
     */
    private Long reply;
    /**
     * 是否解决
     */
    private String solve;
    /**
     * 回复人昵称
     */
    private String replyname;
    /**
     * 回复日期
     */
    private java.util.Date replytime;
}

