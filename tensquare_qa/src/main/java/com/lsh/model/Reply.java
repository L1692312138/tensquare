package com.lsh.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 9:37 上午
 * @desc ：
 */
@Data
@Entity
@Table(name="tb_reply")
public class Reply implements Serializable {

    @Id
    private String id;
    /**
     * 问题ID
     */
    private String problemid;
    /**
     * 回答内容
     */
    private String content;
    private java.util.Date createtime;
    private java.util.Date updatetime;
    /**
     * 回答人ID
     */
    private String userid;
    /**
     * 回答人昵称
     */
    private String nickname;

}